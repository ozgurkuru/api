#!/usr/bin/bash

echo "Making required directories\n"
mkdir -p /var/lib/api/cache
mkdir -p /etc/api

echo "Installing required files\n"
cp repocache.ini /var/lib/api/cache/
cp api.ini /etc/api/
cp api /usr/bin/api

echo "Setting permissions\n"
chmod a+x /usr/bin/api
chmod 777 -R /var/lib/api/cache

